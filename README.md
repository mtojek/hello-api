# README #

## Getting started ##

How to build & run **hello-api** project.
GOPATH can be set to anything (may be a fresh, clean directory). Dependencies are not being versioned currently.

```
go get bitbucket.org/mtojek/hello-api
hello-api
```

*hello-api* will be created in $GOPATH/bin directory.

## Features ##
* go get'table
* Supports clean code rules based on *go tool vet*
* Dependency injection from facebook
* Gracefully managed services
* HTTP framework supported by labstack/echo (minimizing memory footprint)
* Healthcheck
* Heartbeat
* Ping
* Runtime
* Configuration

## Dev ##
* *make all* downloads deps, formats, rebuilds code, clean code check
* *make dev* formats, rebuilds code

## Must do ##

* View Makefile and useful set of commands
* *cat services/application/httpservers/internal_http_server.go*
* *cat services/application/httpservers/external_http_server.go* (empty right now)