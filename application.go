package main

import (
	"bitbucket.org/mtojek/hello-api/core/log"
	"bitbucket.org/mtojek/hello-api/core/services"
	"bitbucket.org/mtojek/hello-api/core/version"
	"bitbucket.org/mtojek/hello-api/services/application/healthcheck"
	"bitbucket.org/mtojek/hello-api/services/application/heartbeat"
	"bitbucket.org/mtojek/hello-api/services/application/httpservers"
	"github.com/facebookgo/inject"
)

const (
	applicationVersion = "0.1"
	applicationName    = "hello-api"
)

type application struct {
	Version            *version.Version                `inject:""`
	ServiceManager     *services.ServiceManager        `inject:""`
	ExternalHTTPServer *httpservers.ExternalHTTPServer `inject:""`
	InternalHTTPServer *httpservers.InternalHTTPServer `inject:""`
	HealthCheck        *healthcheck.HealthCheck        `inject:""`
	Heartbeat          *heartbeat.Heartbeat            `inject:""`
}

func (a *application) start() {
	a.setApplicationVersion()
	a.printWelcome()
	a.startServices()
	a.bye()
}

func (a *application) setApplicationVersion() {
	a.Version.SetOnce(applicationVersion)
}

func (a *application) printWelcome() {
	log.Info("Starting %v v%v.", applicationName, a.Version.ApplicationVersion())
	log.Info(" _          _ _                        _")
	log.Info("| |        | | |                      (_)")
	log.Info("| |__   ___| | | ___ ______ __ _ _ __  _")
	log.Info("| '_ \\ / _ \\ | |/ _ \\______/ _` | '_ \\| |")
	log.Info("| | | |  __/ | | (_) |    | (_| | |_) | |")
	log.Info("|_| |_|\\___|_|_|\\___/      \\__,_| .__/|_|")
	log.Info("                                | |")
	log.Info("                                |_|")
}

func (a *application) startServices() {
	a.ServiceManager.Register(10, a.Heartbeat)
	a.ServiceManager.Register(89, a.HealthCheck)
	a.ServiceManager.Register(90, a.ExternalHTTPServer)
	a.ServiceManager.Register(90, a.InternalHTTPServer)
	a.ServiceManager.Boot()
}

func (a *application) bye() {
	log.Info("Bye, bye!")
}

func main() {
	app := new(application)
	if error := inject.Populate(app); nil != error {
		log.Fatalf("Error occurred while populating dependency graph: %v", error)
	}
	app.start()
}
