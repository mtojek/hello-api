package services

import "sort"

// ManageableServices provides a map with services linked with different levels.
type ManageableServices map[int][]Manageable

func (m ManageableServices) add(level int, service Manageable) {
	m.withInitializedLevel(level)
	m[level] = append(m[level], service)
}

func (m ManageableServices) withInitializedLevel(level int) {
	if _, exists := m[level]; !exists {
		m[level] = []Manageable{}
	}
}

func (m ManageableServices) sortedLevels(sorting levelSorting) []int {
	var keys []int
	for k := range m {
		keys = append(keys, k)
	}

	if sorting == ascending {
		sort.Sort(ascendingLevelSlice(keys))
	} else {
		sort.Sort(descendingLevelSlice(keys))
	}

	return keys
}
