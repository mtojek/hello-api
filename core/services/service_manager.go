package services

import (
	"os"
	"os/signal"
	"syscall"

	"bytes"

	"bitbucket.org/mtojek/hello-api/core/log"
)

// ServiceManager is responsible for registering, booting up and stopping services.
type ServiceManager struct {
	Services ManageableServices `inject:"private"`
}

// Register method registers a manageable service on a defined level.
func (m *ServiceManager) Register(level int, service Manageable) {
	m.Services.add(level, service)
}

// Boot method boots up the whole infrastructure.
func (m *ServiceManager) Boot() {
	m.startServices()
	m.closeOnSignal()
}

func (m *ServiceManager) closeOnSignal() {
	m.waitOnSignal()
	m.stopServices()
}

func (m *ServiceManager) listServices() {
	for _, level := range m.Services.sortedLevels(ascending) {
		var aList bytes.Buffer
		for i, service := range m.Services[level] {
			aList.WriteString(service.Name())
			if i != (len(m.Services[level]) - 1) {
				aList.WriteString(", ")
			}
		}
		log.Info("Level %v: %v", level, aList.String())
	}
}

func (m *ServiceManager) startServices() {
	log.Info("Start registered services.")

	m.listServices()

	for _, level := range m.Services.sortedLevels(ascending) {
		m.startServicesOnLevel(level, m.Services[level])
	}
}

func (m *ServiceManager) waitOnSignal() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	log.Info("Waiting on signal to stop services.")
	<-sig
	log.Info("Signal received.")
}

func (m *ServiceManager) stopServices() {
	log.Info("Stop services gracefully.")

	for _, level := range m.Services.sortedLevels(descending) {
		m.stopServicesOnLevel(level, m.Services[level])
	}
}

func (m *ServiceManager) startServicesOnLevel(level int, services []Manageable) {
	log.Debug("Start services on level %v.", level)

	for _, service := range services {
		log.Debug(`Start service "%v".`, service.Name())
		service.Start()
	}
}

func (m *ServiceManager) stopServicesOnLevel(level int, services []Manageable) {
	log.Debug("Stop services on level %v.", level)

	for _, service := range services {
		service.Stop()
		log.Debug(`Service "%v" has been stopped.`, service.Name())
	}
}
