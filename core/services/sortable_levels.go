package services

type levelSorting int

const (
	ascending levelSorting = iota
	descending
)

type ascendingLevelSlice []int

func (l ascendingLevelSlice) Len() int           { return len(l) }
func (l ascendingLevelSlice) Less(i, j int) bool { return l[i] < l[j] }
func (l ascendingLevelSlice) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }

type descendingLevelSlice []int

func (l descendingLevelSlice) Len() int           { return len(l) }
func (l descendingLevelSlice) Less(i, j int) bool { return l[i] > l[j] }
func (l descendingLevelSlice) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
