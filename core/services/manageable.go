package services

// Manageable interface represents a named entity which can be started or stopped.
type Manageable interface {
	Name() string
	Start()
	Stop()
}
