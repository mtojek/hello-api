package log

import (
	"bytes"
	"fmt"
	golog "log"
	"os"
	"strings"

	"github.com/facebookgo/stack"
)

const (
	debugTag        = "DEBUG"
	infoTag         = "INFO "
	warningTag      = "WARN "
	errorTag        = "ERROR"
	fatalTag        = "FATAL"
	fileLinePattern = " [%s:%d] "
)

var log = golog.New(os.Stderr, "", golog.LstdFlags)

// Debug method logs entry tagged as DEBUG.
func Debug(format string, args ...interface{}) {
	logEntry(debugTag, format, args...)
}

// Info method logs entry tagged as INFO.
func Info(format string, args ...interface{}) {
	logEntry(infoTag, format, args...)
}

// Warn method logs entry tagged as WARN.
func Warn(format string, args ...interface{}) {
	logEntry(warningTag, format, args...)
}

// Errorf method logs entry tagged as ERROR.
func Errorf(format string, args ...interface{}) {
	logEntry(errorTag, format, args...)
}

// Fatalf method logs entry tagged as FATAL and closes the app.
func Fatalf(format string, args ...interface{}) {
	logEntry(fatalTag, format, args...)
	os.Exit(1)
}

func logEntry(tag string, format string, args ...interface{}) {
	frame := stack.Caller(2)
	filename := frame.File[strings.LastIndex(frame.File, "/")+1:]

	var buffer bytes.Buffer
	buffer.WriteString(tag)
	buffer.WriteString(fmt.Sprintf(fileLinePattern, filename, frame.Line))
	buffer.WriteString(format)
	buffer.WriteByte('\n')

	golog.Printf(buffer.String(), args...)
}
