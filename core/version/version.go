package version

import "sync"

// Version is responsible for setting and providing application version.
type Version struct {
	applicationVersion string

	once sync.Once
}

// ApplicationVersion method provides current application version.
func (v *Version) ApplicationVersion() string {
	return v.applicationVersion
}

// SetOnce method set once the application version.
func (v *Version) SetOnce(applicationVersion string) {
	v.once.Do(func() {
		v.applicationVersion = applicationVersion
	})
}
