package graceful

import (
	"net/http"
	"strings"
	"time"

	"bitbucket.org/mtojek/hello-api/core/log"
	reuseport "github.com/kavu/go_reuseport"
	"github.com/tylerb/graceful"
)

const closingTimeout = 5 * time.Second

// Server wraps a graceful server instance with some basic configuration.
type Server struct {
	gracefulServer *graceful.Server
}

// ListenAndServe method creates a server and start listening.
func (s *Server) ListenAndServe(name string, httpServer *http.Server) {
	s.createServer(httpServer)
	s.listenAndServe(name)
}

// Stop method stops the graceful server.
func (s *Server) Stop() {
	s.gracefulServer.Stop(closingTimeout)
	<-s.gracefulServer.StopChan()
}

func (s *Server) createServer(httpServer *http.Server) {
	s.gracefulServer = &graceful.Server{
		Server:           httpServer,
		NoSignalHandling: true,
	}
}

func (s *Server) listenAndServe(name string) {
	log.Debug(`HTTP server "%v" is listening now.`, name)

	if error := s.createListenerAndServe(); nil != error {
		s.mustHandleError(name, error)
	}
}

func (s *Server) createListenerAndServe() error {
	listener, error := reuseport.NewReusablePortListener("tcp4", s.readAddress())
	if nil != error {
		return error
	}

	error = s.gracefulServer.Serve(listener)
	if nil != error {
		return error
	}

	return nil
}

func (s *Server) readAddress() string {
	address := s.gracefulServer.Addr
	if address == "" {
		address = ":http"
	}
	return address
}

func (s *Server) mustHandleError(name string, error error) {
	if !strings.Contains(error.Error(), "use of closed network connection") {
		log.Fatalf(`Error occurred while listening and running HTTP server "%v": %v`, name, error)
	}
}
