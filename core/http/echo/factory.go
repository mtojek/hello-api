package echo

import (
	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
)

// Factory is responsible for creating new initialized Echo web services.
type Factory struct {
	Server *Server `inject:""`
}

// Create method creates a new Echo and appends few basic middlewares.
func (e *Factory) Create(serverName string) *echo.Echo {
	instance := echo.New()
	instance.Use(
		accessLogger(serverName),
		e.Server.header(),
		mw.Gzip(),
		mw.Recover(),
	)
	instance.SetHTTPErrorHandler(httpErrorHandler)
	return instance
}
