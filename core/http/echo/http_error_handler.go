package echo

import (
	"net/http"

	"bitbucket.org/mtojek/hello-api/core/log"
	"github.com/labstack/echo"
)

func httpErrorHandler(err error, c *echo.Context) {
	code := http.StatusInternalServerError
	msg := http.StatusText(code)
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code()
		msg = he.Error()
	}

	if code >= 500 {
		log.Errorf("%v", err)
	}

	if !c.Response().Committed() {
		http.Error(c.Response(), msg, code)
	}
}
