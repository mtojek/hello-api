package echo

import (
	"log"
	"os"
	"time"

	"bitbucket.org/mtojek/hello-api/core/http/request"
	"github.com/labstack/echo"
)

func accessLogger(serverName string) echo.MiddlewareFunc {
	logger := log.New(os.Stdout, "", log.LstdFlags)
	return func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(c *echo.Context) error {
			req := c.Request()
			res := c.Response()

			start := time.Now()
			if err := h(c); err != nil {
				c.Error(err)
			}
			stop := time.Now()
			method := req.Method
			path := req.URL.Path
			if path == "" {
				path = "/"
			}

			logger.Printf("%s %s %s %s %d %s %d", serverName, request.IP(req), method, path, res.Status(), stop.Sub(start), res.Size())
			return nil
		}
	}
}
