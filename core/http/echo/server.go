package echo

import (
	"fmt"

	"bitbucket.org/mtojek/hello-api/core/version"
	"github.com/labstack/echo"
)

const headerServerName = "data-api"

// Server provides a middleware which appends a Server header to a HTTP response.
type Server struct {
	Version *version.Version `inject:""`
}

func (s *Server) header() echo.MiddlewareFunc {
	header := fmt.Sprintf("%v/%v", headerServerName, s.Version.ApplicationVersion())
	return func(h echo.HandlerFunc) echo.HandlerFunc {
		return func(c *echo.Context) error {
			c.Response().Header().Add("Server", header)

			if err := h(c); err != nil {
				c.Error(err)
			}
			return nil
		}
	}
}
