all: go-get-tools build test done

go-get-tools:
	go get golang.org/x/tools/cmd/vet
	go get github.com/golang/lint/golint
	go get golang.org/x/tools/cmd/goimports

build: cc
	go get -t -v ./...

test:
	go test -v ./...
	go test -race  -i ./...
	golint ./... && test -z "`golint ./...`"
	go tool vet -v=true . | grep -v "Checking file"	|| true
	test -z "`find . -type f -name "*.go" | xargs gofmt -s -w`"
	test -z "`find . -type f -name "*.go" | xargs goimports -w`"

cc: #cleancode
	find . -type f -name "*.go" | xargs gofmt -s -w
	find . -type f -name "*.go" | xargs goimports -w

dev: build
	hello-api	

done:
	echo "\n[ Build successful ]\n"
