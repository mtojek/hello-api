package healthcheck

import (
	"bitbucket.org/mtojek/hello-api/services/application/healthcheck/checks"
	"bitbucket.org/mtojek/hello-api/services/application/healthcheck/scheduler"
)

const serviceName = "HealthCheck"

// HealthCheck is responsible for managing health checks.
type HealthCheck struct {
	Scheduler *scheduler.Scheduler `inject:""`

	InternetConnectionCheckFactory *checks.InternetConnectionCheckFactory `inject:""`
}

// CurrentState method returns current state of checks.
func (h *HealthCheck) CurrentState() *scheduler.State {
	return h.Scheduler.CurrentState()
}

// Start method starts the health check service.
func (h *HealthCheck) Start() {
	h.addHealthChecks()
	h.Scheduler.Start()
}

func (h *HealthCheck) addHealthChecks() {
	h.Scheduler.AddJob("@every 5s", h.InternetConnectionCheckFactory.Create("wp_internet_connection", "http://www.wp.pl/"))
}

// Stop method stops the health check service.
func (h *HealthCheck) Stop() {
	h.Scheduler.Stop()
}

// Name method provides the service name.
func (h *HealthCheck) Name() string {
	return serviceName
}
