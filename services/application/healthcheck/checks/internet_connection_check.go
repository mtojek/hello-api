package checks

import (
	"net/http"
	"time"
)

// InternetConnectionCheck is a generic job continuously checking the Internet access (using a specified URL).
type InternetConnectionCheck struct {
	name       string
	requestURL string

	client *http.Client
}

func createInternetConnectionCheck(name string, timeout time.Duration, requestURL string) *InternetConnectionCheck {
	client := &http.Client{
		Timeout: timeout,
	}

	return &InternetConnectionCheck{
		name:       name,
		client:     client,
		requestURL: requestURL,
	}
}

// Check method performs the proper connection check.
func (i *InternetConnectionCheck) Check() error {
	response, error := i.client.Get(i.requestURL)
	defer i.closeConnection(response, error)
	return error
}

func (i *InternetConnectionCheck) closeConnection(response *http.Response, error error) {
	if nil == error {
		response.Body.Close()
	}
}

// Name method returns the check name.
func (i *InternetConnectionCheck) Name() string {
	return i.name
}
