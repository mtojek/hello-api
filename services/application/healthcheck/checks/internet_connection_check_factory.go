package checks

import "time"

const httpClientTimeout = 5 * time.Second

// InternetConnectionCheckFactory is responsible for creating new instances of internet connection checks.
type InternetConnectionCheckFactory struct{}

// Create method creates new internet connection checks.
func (i *InternetConnectionCheckFactory) Create(name string, requestURL string) *InternetConnectionCheck {
	return createInternetConnectionCheck(name, httpClientTimeout, requestURL)
}
