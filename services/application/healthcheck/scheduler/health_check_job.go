package scheduler

import (
	"bitbucket.org/mtojek/hello-api/core/log"
)

type healthCheckJob struct {
	checkable checkable
	result    chan checkResult
}

func (h *healthCheckJob) Run() {
	error := h.checkable.Check()
	h.logError(error)
	h.storeResult(error)
}

func (h *healthCheckJob) logError(error error) {
	if nil != error {
		log.Errorf(`Error occurred while running check "%v": %v`, h.name(), error)
	}
}

func (h *healthCheckJob) storeResult(error error) {
	h.result <- checkResult{name: h.checkable.Name(), error: error}
}

func (h *healthCheckJob) name() string {
	return h.checkable.Name()
}
