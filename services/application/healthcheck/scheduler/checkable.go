package scheduler

type checkable interface {
	Check() error
	Name() string
}
