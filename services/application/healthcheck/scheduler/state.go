package scheduler

// State corresponds to the application health status.
type State struct {
	Healthy bool             `json:"healthy"`
	Checks  map[string]Check `json:"checks"`
}

// Check corresponds to a particular health test case.
type Check struct {
	Healthy  bool   `json:"healthy"`
	LastTime string `json:"lastTime"`
	Error    string `json:"error,omitempty"`
}

// IsHealthy method says if the entire running application is healthy.
func (s *State) IsHealthy() bool {
	return s.Healthy
}
