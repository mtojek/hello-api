package scheduler

type namedJob interface {
	name() string
}
