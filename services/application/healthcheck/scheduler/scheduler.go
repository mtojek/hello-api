package scheduler

import (
	"sync"

	"github.com/robfig/cron"
)

// Scheduler is responsible for starting, stopping cron instance, adding jobs and returning health current state.
type Scheduler struct {
	StateStore         *StateStore         `inject:""`
	EntryStateComposer *EntryStateComposer `inject:""`
	cron               *cron.Cron

	once sync.Once
}

// AddJob method add a new job to the cron instance.
func (c *Scheduler) AddJob(SchedulerSpec string, checkable checkable) {
	c.initializeOnce()
	c.cron.AddJob(SchedulerSpec, &healthCheckJob{result: c.StateStore.stateChannel(), checkable: checkable})
}

// Start method starts the component and its internals.
func (c *Scheduler) Start() {
	c.initializeOnce()
	c.StateStore.start()
	c.cron.Start()
}

// Stop method stops the component and its internals.
func (c *Scheduler) Stop() {
	c.cron.Stop()
	c.StateStore.stop()
}

// CurrentState method freezes health checks states and returns them to the caller.
func (c *Scheduler) CurrentState() *State {
	entries := c.cron.Entries()
	states := c.StateStore.status()
	return c.EntryStateComposer.compose(entries, states)
}

func (c *Scheduler) initializeOnce() {
	c.once.Do(c.initialize)
}

func (c *Scheduler) initialize() {
	c.cron = cron.New()
}
