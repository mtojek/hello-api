package scheduler

import (
	"sync"
)

const stateChanSize = 128

// StateStore is responsible for managing health check states.
type StateStore struct {
	stopChan           chan bool
	stateChan          chan checkResult
	statusRequestChan  chan bool
	statusResponseChan chan map[string]error

	states map[string]error

	once sync.Once
}

func (s *StateStore) start() {
	s.initializeOnce()
	go s.run()
}

func (s *StateStore) initializeOnce() {
	s.once.Do(s.initialize)
}

func (s *StateStore) initialize() {
	s.stopChan = make(chan bool)
	s.statusRequestChan = make(chan bool)
	s.statusResponseChan = make(chan map[string]error)
	s.stateChan = make(chan checkResult, stateChanSize)
	s.states = map[string]error{}
}

func (s *StateStore) run() {
	for {
		select {
		case <-s.stopChan:
			return
		case state := <-s.stateChan:
			s.states[state.name] = state.error
		case <-s.statusRequestChan:
			s.statusResponseChan <- s.freezeStates()
		}
	}
}

func (s *StateStore) freezeStates() map[string]error {
	frozen := map[string]error{}
	for k, v := range s.states {
		frozen[k] = v
	}
	return frozen
}

func (s *StateStore) stop() {
	s.stopChan <- true
}

func (s *StateStore) status() map[string]error {
	s.statusRequestChan <- true
	return <-s.statusResponseChan
}

func (s *StateStore) stateChannel() chan checkResult {
	s.initializeOnce()
	return s.stateChan
}
