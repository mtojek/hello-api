package scheduler

import (
	"time"

	"github.com/robfig/cron"
)

const (
	statusNotReceivedYet     = "Status not received yet."
	healthCheckJobNotUpdated = "Health check has not been updated for a longer period of time."
	lastTimeFormat           = "2006-01-02 15:04:05"
)

// EntryStateComposer is responsible for composing cron entries with corresponding health checks statuses.
type EntryStateComposer struct{}

func (e *EntryStateComposer) compose(entries []*cron.Entry, states map[string]error) *State {
	state := new(State)
	state.Checks = map[string]Check{}

	healthy := true

	for _, entry := range entries {
		if job, ok := entry.Job.(namedJob); ok {
			name := job.name()

			var errorMessage string
			isHealthy := false
			lastTime := e.formatLastTime(entry.Prev)

			if entry.Prev.Before(time.Now().Add(-1 * time.Minute)) {
				errorMessage = healthCheckJobNotUpdated
			} else if error, exists := states[name]; exists {
				isHealthy = (nil == error)
				errorMessage = e.errorMessage(error)
			} else {
				errorMessage = statusNotReceivedYet
			}

			healthy = healthy && isHealthy
			state.Checks[name] = Check{
				Healthy:  isHealthy,
				LastTime: lastTime,
				Error:    errorMessage,
			}
		}
	}

	state.Healthy = healthy
	return state
}

func (e *EntryStateComposer) errorMessage(error error) string {
	if nil == error {
		return ""
	}
	return error.Error()
}

func (e *EntryStateComposer) formatLastTime(lastTime time.Time) string {
	return lastTime.Format(lastTimeFormat)
}
