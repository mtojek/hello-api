package handlers

import (
	"net/http"

	"bitbucket.org/mtojek/hello-api/core/log"
	"bitbucket.org/mtojek/hello-api/services/application/configuration"
	"github.com/labstack/echo"
)

// ConfigurationHandler is responsible for serving the current application configuration.
type ConfigurationHandler struct {
	ConfigurationProvider *configuration.Provider `inject:""`
}

// Handle method writes the application configuration.
func (ch *ConfigurationHandler) Handle(c *echo.Context) error {
	log.Debug("Fetch application configuration")
	configuration := ch.ConfigurationProvider.Get()
	return c.JSONIndent(http.StatusOK, configuration, " ", " ")
}
