package handlers

import (
	"fmt"
	"net/http"
	"os"
	"os/user"
	"runtime"
	"time"

	"bitbucket.org/mtojek/hello-api/core/log"
	"github.com/kardianos/osext"
	"github.com/labstack/echo"
)

const runtimeTimeNowFormat = "2006-01-02 15:04:05"

type runtimeData struct {
	HostData        hostData        `json:"host"`
	EnvironmentData environmentData `json:"environment"`
	UserData        userData        `json:"user"`
	ProcessData     processData     `json:"process"`
	ApplicationData applicationData `json:"application"`
}

type hostData struct {
	Hostname string `json:"hostname"`
	Time     string `json:"time"`
}

type environmentData struct {
	GoMaxProcs int    `json:"GOMAXPROCS"`
	GoRoot     string `json:"GOROOT"`
}

type userData struct {
	UID      string `json:"uid"`
	GID      string `json:"gid"`
	Username string `json:"username"`
	Name     string `json:"name"`
	HomeDir  string `json:"homeDir"`
}

type processData struct {
	PID     int    `json:"pid"`
	PPID    int    `json:"ppid"`
	Name    string `json:"name"`
	Path    string `json:"path"`
	WorkDir string `json:"workDir"`
}

type applicationData struct {
	NumGoroutine int `json:"numGoroutine"`
}

// RuntimeHandler is responsible for serving runtime data.
type RuntimeHandler struct{}

// Handle methods writes data related to Go runtime.
func (r *RuntimeHandler) Handle(c *echo.Context) error {
	log.Debug("Fetch runtime data")

	response := &runtimeData{
		HostData:        r.createHostData(),
		EnvironmentData: r.createEnvironmentData(),
		UserData:        r.createUserData(),
		ProcessData:     r.createProcessData(),
		ApplicationData: r.createApplicationData(),
	}
	return c.JSONIndent(http.StatusOK, response, " ", " ")
}

func (r *RuntimeHandler) createHostData() hostData {
	hostname, error := os.Hostname()
	if nil != error {
		hostname = r.formatError(error)
	}

	return hostData{
		Hostname: hostname,
		Time:     time.Now().Format(runtimeTimeNowFormat),
	}
}

func (r *RuntimeHandler) createEnvironmentData() environmentData {
	return environmentData{
		GoMaxProcs: runtime.GOMAXPROCS(-1),
		GoRoot:     runtime.GOROOT(),
	}
}

func (r *RuntimeHandler) createUserData() userData {
	currentUser, error := user.Current()
	if nil != error {
		errorMessage := r.formatError(error)
		return userData{
			UID:      errorMessage,
			GID:      errorMessage,
			Username: errorMessage,
			Name:     errorMessage,
			HomeDir:  errorMessage,
		}
	}
	return userData{
		UID:      currentUser.Uid,
		GID:      currentUser.Gid,
		Username: currentUser.Username,
		Name:     currentUser.Name,
		HomeDir:  currentUser.HomeDir,
	}
}

func (r *RuntimeHandler) createProcessData() processData {
	workDir, error := os.Getwd()
	if nil != error {
		workDir = r.formatError(error)
	}

	executableFolder, error := osext.ExecutableFolder()
	if nil != error {
		executableFolder = r.formatError(error)
	}

	return processData{
		PID:     os.Getpid(),
		PPID:    os.Getppid(),
		Name:    os.Args[0],
		Path:    executableFolder,
		WorkDir: workDir,
	}
}

func (r *RuntimeHandler) createApplicationData() applicationData {
	return applicationData{
		NumGoroutine: runtime.NumGoroutine(),
	}
}

func (r *RuntimeHandler) formatError(error error) string {
	return fmt.Sprintf("Error: %v", error.Error())
}
