package handlers

import (
	"net/http"

	"github.com/labstack/echo"
)

// TrailingSlashRedirectHandler is responsible for HTTP redirection to base resources.
type TrailingSlashRedirectHandler struct{}

// Handle methods redirects permanently to resources with appended '/' (slash).
func (r *TrailingSlashRedirectHandler) Handle(c *echo.Context) error {
	url := *c.Request().URL
	url.Path = url.Path + "/"

	http.Redirect(c.Response(), c.Request(), url.String(), http.StatusMovedPermanently)
	return nil
}
