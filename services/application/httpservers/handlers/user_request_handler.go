package handlers

import (
	"net/http"

	"bitbucket.org/mtojek/hello-api/core/http/request"
	"bitbucket.org/mtojek/hello-api/core/log"
	"github.com/labstack/echo"
)

type userRequest struct {
	IP        string `json:"ip"`
	UserAgent string `json:"userAgent"`
}

// UserRequestHandler is responsible for serving user request data.
type UserRequestHandler struct{}

// Handle method writes data related to user request.
func (u *UserRequestHandler) Handle(c *echo.Context) error {
	log.Debug("Fetch user request data")

	userRequest := &userRequest{
		IP:        request.IP(c.Request()),
		UserAgent: c.Request().UserAgent(),
	}
	return c.JSONIndent(http.StatusOK, userRequest, " ", " ")
}
