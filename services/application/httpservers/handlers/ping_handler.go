package handlers

import (
	"net/http"

	"bitbucket.org/mtojek/hello-api/core/log"
	"github.com/labstack/echo"
)

var pong = struct {
	Response string `json:"response"`
}{
	Response: "pong",
}

// PingHandler is responsible for serving ping-pong checks.
type PingHandler struct{}

// Handle method writes a standard pong response.
func (p *PingHandler) Handle(c *echo.Context) error {
	log.Debug("Ping-pong")
	return c.JSON(http.StatusOK, pong)
}
