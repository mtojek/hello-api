package handlers

import (
	"net/http"

	"bitbucket.org/mtojek/hello-api/core/log"
	"bitbucket.org/mtojek/hello-api/services/application/healthcheck"
	"github.com/labstack/echo"
)

// HealthCheckHandler is responsible for serving health status of the application.
type HealthCheckHandler struct {
	HealthCheck *healthcheck.HealthCheck `inject:""`
}

type healthyState interface {
	IsHealthy() bool
}

// Handle method provides health status data and sends appropriate HTTP status code.
func (h *HealthCheckHandler) Handle(c *echo.Context) error {
	log.Debug("Fetch current health check state.")
	state := h.HealthCheck.CurrentState()
	status := h.mapHTTPStatus(state)
	c.JSONIndent(status, state, " ", " ")
	return nil
}

func (h *HealthCheckHandler) mapHTTPStatus(state healthyState) int {
	if state.IsHealthy() {
		return http.StatusOK
	}
	return http.StatusInternalServerError
}
