package handlers

import (
	"net/http"

	"bitbucket.org/mtojek/hello-api/core/log"
	"bitbucket.org/mtojek/hello-api/core/version"
	"github.com/labstack/echo"
)

// VersionHandler is responsible for serving the application version.
type VersionHandler struct {
	Version *version.Version `inject:""`
}

type applicationVersion struct {
	ApplicationVersion string `json:"applicationVersion"`
}

// Handle method writes the application version.
func (v *VersionHandler) Handle(c *echo.Context) error {
	log.Debug("Fetch application version")
	return c.JSON(http.StatusOK, v.fetchApplicationVersion())
}

func (v *VersionHandler) fetchApplicationVersion() *applicationVersion {
	return &applicationVersion{ApplicationVersion: v.Version.ApplicationVersion()}
}
