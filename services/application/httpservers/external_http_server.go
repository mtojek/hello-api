package httpservers

import (
	"net/http"

	"bitbucket.org/mtojek/hello-api/core/http/echo"
	"bitbucket.org/mtojek/hello-api/core/http/graceful"
	"bitbucket.org/mtojek/hello-api/services/application/configuration"
)

const (
	externalHTTPShortServerName = "external"
	externalHTTPLongServerName  = "ExternalHTTPServer"
)

// ExternalHTTPServer is externally available HTTP server.
type ExternalHTTPServer struct {
	graceful.Server

	EchoFactory           *echo.Factory           `inject:""`
	ConfigurationProvider *configuration.Provider `inject:""`
}

// Start method creates and starts the external HTTP server.
func (ex *ExternalHTTPServer) Start() {
	go ex.ListenAndServe(externalHTTPShortServerName, ex.createNamedHTTPServer())
}

func (ex *ExternalHTTPServer) createNamedHTTPServer() *http.Server {
	e := ex.EchoFactory.Create(externalHTTPShortServerName)
	return e.Server(ex.ConfigurationProvider.Get().ExternalIPPort)
}

// Name method provides the external server name.
func (ex *ExternalHTTPServer) Name() string {
	return externalHTTPLongServerName
}
