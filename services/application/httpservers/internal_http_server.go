package httpservers

import (
	"net/http"

	"bitbucket.org/mtojek/hello-api/core/http/echo"
	"bitbucket.org/mtojek/hello-api/core/http/graceful"
	"bitbucket.org/mtojek/hello-api/services/application/configuration"
	"bitbucket.org/mtojek/hello-api/services/application/httpservers/handlers"
	"github.com/echo-contrib/echopprof"
)

const (
	internalHTTPShortServerName = "internal"
	internalHTTPLongServerName  = "InternalHTTPServer"
)

// InternalHTTPServer is internally available HTTP server.
type InternalHTTPServer struct {
	graceful.Server

	EchoFactory           *echo.Factory           `inject:""`
	ConfigurationProvider *configuration.Provider `inject:""`

	PingHandler                  *handlers.PingHandler                  `inject:""`
	RuntimeHandler               *handlers.RuntimeHandler               `inject:""`
	UserRequestHandler           *handlers.UserRequestHandler           `inject:""`
	VersionHandler               *handlers.VersionHandler               `inject:""`
	ConfigurationHandler         *handlers.ConfigurationHandler         `inject:""`
	TrailingSlashRedirectHandler *handlers.TrailingSlashRedirectHandler `inject:""`
	HealthCheckHandler           *handlers.HealthCheckHandler           `inject:""`
}

// Start method starts the internal HTTP server.
func (i *InternalHTTPServer) Start() {
	go i.ListenAndServe(internalHTTPShortServerName, i.createNamedHTTPServer())
}

func (i *InternalHTTPServer) createNamedHTTPServer() *http.Server {
	e := i.EchoFactory.Create(internalHTTPShortServerName)
	e.Get("/ping", i.PingHandler.Handle)
	e.Get("/runtime", i.RuntimeHandler.Handle)
	e.Get("/user", i.UserRequestHandler.Handle)
	e.Get("/version", i.VersionHandler.Handle)
	e.Get("/configuration", i.ConfigurationHandler.Handle)
	e.Get("/healthcheck", i.HealthCheckHandler.Handle)

	e.Get("/pprof", i.TrailingSlashRedirectHandler.Handle)
	e.Get("/pprof/", echopprof.IndexHandler)
	e.Get("/pprof/heap", echopprof.HeapHandler)
	e.Get("/pprof/goroutine", echopprof.GoroutineHandler)
	e.Get("/pprof/block", echopprof.BlockHandler)
	e.Get("/pprof/threadcreate", echopprof.ThreadCreateHandler)
	e.Get("/pprof/cmdline", echopprof.CmdlineHandler)
	e.Get("/pprof/profile", echopprof.ProfileHandler)
	e.Get("/pprof/symbol", echopprof.SymbolHandler)

	return e.Server(i.ConfigurationProvider.Get().InternalIPPort)
}

// Name method provides the internal server name.
func (i *InternalHTTPServer) Name() string {
	return internalHTTPLongServerName
}
