package configuration

// ApplicationConfiguration contains basic application configuration.
type ApplicationConfiguration struct {
	ExternalIPPort string `envconfig:"EXTERNAL_IP_PORT" required:"true" default:"0.0.0.0:9000" json:"EXTERNAL_IP_PORT"`
	InternalIPPort string `envconfig:"INTERNAL_IP_PORT" required:"true" default:"0.0.0.0:8000" json:"INTERNAL_IP_PORT"`
}
