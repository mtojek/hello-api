package configuration

import (
	"fmt"
	"net"
	"strconv"
	"strings"
)

// Validator is responsible for checking loaded configuration.
type Validator struct{}

// Validate method checks the given configuration.
func (v *Validator) Validate(ac *ApplicationConfiguration) error {
	if error := v.validateIPPort(ac.ExternalIPPort); nil != error {
		return error
	}

	if error := v.validateIPPort(ac.InternalIPPort); nil != error {
		return error
	}

	return nil
}

func (v *Validator) validateIPPort(ipPort string) error {
	split := strings.Split(ipPort, ":")
	if len(split) != 2 {
		return fmt.Errorf("invalid format, required \"IPv4:port\", given: %v", ipPort)
	}

	ip := split[0]
	if parsedIP := net.ParseIP(ip); nil == parsedIP {
		return fmt.Errorf("cannot parse IP address, required \"IPv4:port\", given: %v", ipPort)
	}

	port := split[1]
	if parsedPort, error := strconv.ParseUint(port, 10, 64); nil != error || parsedPort > 65535 {
		return fmt.Errorf("cannot parse port (valid range: 1-65535), required \"IPv4:port\", given: %v", ipPort)
	}

	return nil
}
