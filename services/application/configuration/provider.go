package configuration

import (
	"sync"

	"bitbucket.org/mtojek/hello-api/core/log"
	"github.com/kelseyhightower/envconfig"
)

const envPrefix = "DATA_API"

// Provider is responsible for loading and providing configuration.
type Provider struct {
	Validator                *Validator `inject:""`
	applicationConfiguration *ApplicationConfiguration

	once sync.Once
}

// Get method returns a valid configuration of the application.
func (p *Provider) Get() *ApplicationConfiguration {
	p.once.Do(p.provideOnce)
	return p.applicationConfiguration
}

func (p *Provider) provideOnce() {
	p.applicationConfiguration = p.mustReadApplicationConfiguration()
}

func (p *Provider) mustReadApplicationConfiguration() *ApplicationConfiguration {
	ac := new(ApplicationConfiguration)

	error := envconfig.Process(envPrefix, ac)
	if nil != error {
		log.Fatalf("Error occurred while reading configuration from environment: %v", error)
	}

	error = p.Validator.Validate(ac)
	if nil != error {
		log.Fatalf("Error occurred while validating configuration: %v", error)
	}

	return ac
}
