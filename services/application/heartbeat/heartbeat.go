package heartbeat

import (
	"runtime"
	"time"

	"bitbucket.org/mtojek/hello-api/core/log"
)

const serviceName = "Heartbeat"

var heartbeatPeriod = 15 * time.Second

// Heartbeat is a service responsible for reporting its alive status.
type Heartbeat struct {
	stopChannel chan bool
}

// Start method starts the heartbeat service.
func (h *Heartbeat) Start() {
	h.initialize()
	go h.startBeating()
}

func (h *Heartbeat) startBeating() {
	for {
		select {
		case <-h.stopChannel:
			return
		case <-time.After(heartbeatPeriod):
			log.Info("Heartbeat (numGoroutine: %d)", runtime.NumGoroutine())
		}
	}
}

// Stop method stops the heartbeat service.
func (h *Heartbeat) Stop() {
	h.stopChannel <- true
}

func (h *Heartbeat) initialize() {
	h.stopChannel = make(chan bool)
}

// Name method returns the name of the service.
func (h *Heartbeat) Name() string {
	return serviceName
}
